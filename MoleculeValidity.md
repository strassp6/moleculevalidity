To check validity of the molecule there are mainly two ways :

1. Use the SanitizeMol function https://www.rdkit.org/docs/cppapi/namespaceRDKit_1_1MolOps.html#a8d831787aaf2d65d9920c37b25b476f5
2. Transform your molecule to SMILE and then back to molecule. For example of code, check https://bitbucket.org/dmmlgeneva/oopml-molecule-api/src/a65eb63204467582eecabbb723bfcda16954fea1/Molecule/src/Main/EvalLoopRandomIndividual.h#lines-163 and https://bitbucket.org/dmmlgeneva/oopml-molecule-api/src/a65eb63204467582eecabbb723bfcda16954fea1/Molecule/src/Main/Evaluate.h#lines-287 . 
